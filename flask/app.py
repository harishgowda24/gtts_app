from flask_api import FlaskAPI
from flask_cors import CORS
from flask import request
from flask import send_file
from werkzeug.utils import secure_filename
from gtts import gTTS
import os, random, shutil, re
from google.cloud import texttospeech

app = FlaskAPI(__name__)
# app.config['MEDIA'] = 'media'
app.secret_key = 'harish'
app.config['CORS_HEADERS'] = 'Content-Type'
CORS(app)
outputFolder = 'ttsmedia'
def createFolder(path):
    if os.path.exists(path):
        pass
    else:
        os.mkdir(path)
createFolder(outputFolder)

def googleCloudTextToSpeech(text, path):
    os.environ['GOOGLE_APPLICATION_CREDENTIALS']='tts-cloud-api-d3b6717addca.json'
    # Instantiates a client
    client = texttospeech.TextToSpeechClient()
    synthesis_input = texttospeech.SynthesisInput(text=text)
    voice = texttospeech.VoiceSelectionParams(
        language_code="en-IN", ssml_gender=texttospeech.SsmlVoiceGender.NEUTRAL
    )
    audio_config = texttospeech.AudioConfig(
        audio_encoding=texttospeech.AudioEncoding.MP3
    )
    response = client.synthesize_speech(
        input=synthesis_input, voice=voice, audio_config=audio_config
    )
    with open(path, "wb") as out:
        # Write the response to the output file.
        out.write(response.audio_content)
        print('Audio content written to file ',path)

@app.route('/download/<string:userId>&<string:audioName>',methods=['GET', 'POST'])
def download_audio(userId, audioName):
    # print(userId)
    return send_file(outputFolder+'/'+userId+'/'+audioName)

@app.route('/textdatatospeech', methods=['POST'])
def textdata():
    text = request.json['text']
    print(text)
    userId = request.json['sessionUser']
    createFolder(outputFolder+'/'+userId)
    randomNumber = random.randint(1,1000)
    audioFilename = "ssId_"+str(userId)+'_'+str(randomNumber)+'_text.mp3'
    try:
        # myobj = gTTS(text=text, lang='en', slow=False)
        # myobj.save(outputFolder+'/'+userId+'/'+audioFilename)
        googleCloudTextToSpeech(text, outputFolder+'/'+userId+'/'+audioFilename)

    except :
        return {'error': 'No text to speak'}, 500
    return {'audioName' :audioFilename}

@app.route('/textfiletospeech', methods=['POST'])
def textfile():
    try:
        f = request.files['file']
        userId = request.data["sessionUser"]
        createFolder(outputFolder+'/'+userId)
        getfileName = re.sub('[^A-Za-z0-9]+', '_', f.filename.split('.')[0])
        fileName = getfileName+'.'+f.filename.split('.')[1]
        print(fileName)
        outputAudioName = fileName.split('.')[0]+'.mp3'
        if fileName.split('.')[1]=='txt':
            f.save(os.path.join(outputFolder+'/'+str(userId),secure_filename(fileName)))
            print("text file")
            with open(outputFolder+'/'+str(userId)+'/'+fileName, 'r') as reader:
                text = reader.read()
            try:
                # myobj = gTTS(text=text, lang='en', slow=False)
                # myobj.save(outputFolder+'/'+userId+'/'+outputAudioName)
                googleCloudTextToSpeech(text, outputFolder+'/'+userId+'/'+outputAudioName)

            except :
                return {'error': 'No text to speak'}, 500
            return {'audioName' :outputAudioName}
        else:
            return {'error': 'Invalid input file, please uplaod text file only'}, 500
        # print(request.files)
        return "True"
    except :
        return {'error': 'Somthing wrong'}, 500


@app.route('/deletesession', methods=['POST'])
def deleteSession():
    data = request.json['query']
    path = outputFolder+'/'+str(data['id'])
    print(data['id'])
    if os.path.exists(path):
        shutil.rmtree(path)
        return {'message':'item deleted'}, 200
    else:
        return {'message':"item '{}' not found.".format(data['id'])}, 200

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=6060)
